﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScintillaNET;
using NLog;
using RestSharp;

namespace APIMan
{
    public partial class Form1 : Form
    {
        Scintilla outBox;
        BinaryFormatter _serializer = new BinaryFormatter();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rTypeBox.SelectedIndex = 0;

            initCodebox();

            try
            {
                if (File.Exists("save.dat"))
                {
                    using (FileStream strm = File.Open("save.dat", FileMode.Open))
                    {
                        List<Request> requests = (List<Request>)_serializer.Deserialize(strm);

                        foreach (Request req in requests)
                            collectionList.Items.Add(req);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failed to load requests");
            }
        }


        #region Output Box
        private void initCodebox()
        {
            outBox = new Scintilla();
            outBox.Dock = DockStyle.Fill;
            outBox.Lexer = Lexer.Cpp;
            outBox.LexerLanguage = "javascript";
            outBox.Margins[0].Width = 16;
            outBox.TextChanged += OutBox_TextChanged;
            outBox.ReadOnly = true;

            splitContainer1.Panel2.Controls.Add(outBox);
        }

        private int maxLineNumberCharLength;
        private void OutBox_TextChanged(object sender, EventArgs e)
        {
            var maxLineNumberCharLength = outBox.Lines.Count.ToString().Length;
            if (maxLineNumberCharLength == this.maxLineNumberCharLength)
                return;

            const int padding = 2;
            outBox.Margins[0].Width = outBox.TextWidth(Style.LineNumber, new string('9', maxLineNumberCharLength + 1)) + padding;
            this.maxLineNumberCharLength = maxLineNumberCharLength;
        }

        string JsonPrettify(string json)
        {
            return Newtonsoft.Json.Linq.JObject.Parse(json).ToString();
        }

        #endregion

        Request genRequest()
        {
            var name = rNameBox.Text;
            var url = rURLBox.Text;
            var type = rTypeBox.SelectedIndex;

            List<RequestParam> param = new List<RequestParam>();
            foreach (object item in rParamsList.Items)
                param.Add((RequestParam)item);

            Request req = new Request(name);
            req.URL = url;
            req.Type = type;

            req.Params = param;

            return req;
        }

        Request curRequest;
        private void rAddBtn_Click(object sender, EventArgs e)
        {
            Request req = genRequest();

            int ind = -1;
            if (curRequest != null)
                ind = collectionList.Items.IndexOf(curRequest);
            
            if(ind != -1)
            {
                collectionList.Items[ind] = req;
                curRequest = req;
            }
            else
                collectionList.Items.Add(req);

            curRequest = req;

            saveAll();
        }

        private void rParamsAdd_Click(object sender, EventArgs e)
        {
            var name = rParamsName.Text;
            var value = rParamsValue.Text;

            if(name.Trim() == "")
            {
                MessageBox.Show("You must enter a param name.", "Error");
                return;
            }

            rParamsName.Text = "";
            rParamsValue.Text = "";

            RequestParam param = new RequestParam(name, value);
            rParamsList.Items.Add(param);
        }

        void fillValues(Request param)
        {


            rNameBox.Text = param.Name;
            rURLBox.Text = param.URL;
            rTypeBox.SelectedIndex = param.Type;

            rParamsName.Text = "";
            rParamsValue.Text = "";

            rParamsList.Items.Clear();
            foreach (RequestParam p in param.Params)
                rParamsList.Items.Add(p);
        }

        private void collectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var param = (Request)collectionList.SelectedItem;
            if (param == null) return;

            fillValues(param);
            curRequest = param;
        }

        void newReq()
        {
            curRequest = null;
            rNameBox.Text = "";
            rURLBox.Text = "";
            rTypeBox.SelectedIndex = 0;

            rParamsName.Text = "";
            rParamsValue.Text = "";
            rParamsList.Items.Clear();
        }

        /// <summary>
        /// This is actually for the New button
        /// 
        /// I apparently can't tell the difference 
        /// between once that saves
        /// and one that ERASES EVERYTHING :)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rSaveBtn_Click(object sender, EventArgs e)
        {
            newReq();
        }

        private void rRunBtn_Click(object sender, EventArgs e)
        {
            Request req = genRequest();

            try
            {
                var client = new RestClient(req.URL);
                Method met = (req.Type == 0) ? Method.GET: ((req.Type == 1) ? Method.POST : Method.PUT);

                var request = new RestRequest(met);
                foreach(RequestParam param in req.Params)
                    request.AddParameter(param.Name, param.Value);

                IRestResponse response = client.Execute(request);
                if(response.ErrorException != null)
                {
                    MessageBox.Show(response.ErrorException.ToString(), "Request Failed");
                    return;
                }

                var outp = "-No Response-";

                if (response.ContentType == "application/json")
                    outp = "-JSON-\n" + JsonPrettify(response.Content);
                else
                    outp = response.Content;

                outBox.ReadOnly = false;
                outBox.InsertText(0, outp);
                outBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Request Failed");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveAll();
        }

        /// <summary>
        /// Stores our saved requests
        /// </summary>
        void saveAll()
        {
            try
            {
                List<Request> requests = new List<Request>();
                foreach (object req in collectionList.Items)
                    requests.Add((Request)req);

                using (FileStream strm = File.Open("save.dat", FileMode.OpenOrCreate))
                {
                    _serializer.Serialize(strm, requests);
                    strm.Flush();
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex, "Failed to save requests");
            }
        }

        private void rParamsRemove_Click(object sender, EventArgs e)
        {
            int ind = rParamsList.SelectedIndex;
            if (ind < 0) return;
            rParamsList.Items.RemoveAt(ind);
        }

        private void rRemoveBtn_Click(object sender, EventArgs e)
        {
            int ind = collectionList.SelectedIndex;
            if (ind < 0) return;

            collectionList.Items.RemoveAt(ind);

            if (collectionList.Items.Count < 1)
                newReq();
            else
                collectionList.SelectedIndex = ind - 1;
            saveAll();
        }
    }
}

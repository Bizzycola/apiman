﻿namespace APIMan
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.collectionList = new System.Windows.Forms.ListBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rRemoveBtn = new System.Windows.Forms.Button();
            this.rAddBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rParamsRemove = new System.Windows.Forms.Button();
            this.rParamsAdd = new System.Windows.Forms.Button();
            this.rParamsValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rParamsName = new System.Windows.Forms.TextBox();
            this.rParamsList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rTypeBox = new System.Windows.Forms.ComboBox();
            this.rNameBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rURLBox = new System.Windows.Forms.TextBox();
            this.rSaveBtn = new System.Windows.Forms.Button();
            this.rRunBtn = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1139, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 19);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.collectionList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(192, 496);
            this.panel1.TabIndex = 1;
            // 
            // collectionList
            // 
            this.collectionList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.collectionList.FormattingEnabled = true;
            this.collectionList.ItemHeight = 20;
            this.collectionList.Location = new System.Drawing.Point(0, 0);
            this.collectionList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.collectionList.Name = "collectionList";
            this.collectionList.Size = new System.Drawing.Size(192, 496);
            this.collectionList.TabIndex = 3;
            this.collectionList.SelectedIndexChanged += new System.EventHandler(this.collectionList_SelectedIndexChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(192, 25);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rRunBtn);
            this.splitContainer1.Panel1.Controls.Add(this.rSaveBtn);
            this.splitContainer1.Panel1.Controls.Add(this.rRemoveBtn);
            this.splitContainer1.Panel1.Controls.Add(this.rAddBtn);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(947, 496);
            this.splitContainer1.SplitterDistance = 363;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 2;
            // 
            // rRemoveBtn
            // 
            this.rRemoveBtn.Location = new System.Drawing.Point(246, 167);
            this.rRemoveBtn.Name = "rRemoveBtn";
            this.rRemoveBtn.Size = new System.Drawing.Size(85, 31);
            this.rRemoveBtn.TabIndex = 9;
            this.rRemoveBtn.Text = "Remove";
            this.rRemoveBtn.UseVisualStyleBackColor = true;
            this.rRemoveBtn.Click += new System.EventHandler(this.rRemoveBtn_Click);
            // 
            // rAddBtn
            // 
            this.rAddBtn.Location = new System.Drawing.Point(166, 167);
            this.rAddBtn.Name = "rAddBtn";
            this.rAddBtn.Size = new System.Drawing.Size(74, 31);
            this.rAddBtn.TabIndex = 8;
            this.rAddBtn.Text = "Save";
            this.rAddBtn.UseVisualStyleBackColor = true;
            this.rAddBtn.Click += new System.EventHandler(this.rAddBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rParamsRemove);
            this.groupBox2.Controls.Add(this.rParamsAdd);
            this.groupBox2.Controls.Add(this.rParamsValue);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.rParamsName);
            this.groupBox2.Controls.Add(this.rParamsList);
            this.groupBox2.Location = new System.Drawing.Point(422, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 340);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Request Params";
            // 
            // rParamsRemove
            // 
            this.rParamsRemove.Location = new System.Drawing.Point(151, 85);
            this.rParamsRemove.Name = "rParamsRemove";
            this.rParamsRemove.Size = new System.Drawing.Size(78, 31);
            this.rParamsRemove.TabIndex = 6;
            this.rParamsRemove.Text = "Remove";
            this.rParamsRemove.UseVisualStyleBackColor = true;
            this.rParamsRemove.Click += new System.EventHandler(this.rParamsRemove_Click);
            // 
            // rParamsAdd
            // 
            this.rParamsAdd.Location = new System.Drawing.Point(67, 85);
            this.rParamsAdd.Name = "rParamsAdd";
            this.rParamsAdd.Size = new System.Drawing.Size(78, 31);
            this.rParamsAdd.TabIndex = 5;
            this.rParamsAdd.Text = "Add";
            this.rParamsAdd.UseVisualStyleBackColor = true;
            this.rParamsAdd.Click += new System.EventHandler(this.rParamsAdd_Click);
            // 
            // rParamsValue
            // 
            this.rParamsValue.Location = new System.Drawing.Point(67, 53);
            this.rParamsValue.Name = "rParamsValue";
            this.rParamsValue.Size = new System.Drawing.Size(207, 26);
            this.rParamsValue.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Value:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Name:";
            // 
            // rParamsName
            // 
            this.rParamsName.Location = new System.Drawing.Point(67, 25);
            this.rParamsName.Name = "rParamsName";
            this.rParamsName.Size = new System.Drawing.Size(207, 26);
            this.rParamsName.TabIndex = 1;
            // 
            // rParamsList
            // 
            this.rParamsList.Dock = System.Windows.Forms.DockStyle.Right;
            this.rParamsList.FormattingEnabled = true;
            this.rParamsList.ItemHeight = 20;
            this.rParamsList.Location = new System.Drawing.Point(280, 22);
            this.rParamsList.Name = "rParamsList";
            this.rParamsList.Size = new System.Drawing.Size(230, 315);
            this.rParamsList.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rTypeBox);
            this.groupBox1.Controls.Add(this.rNameBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rURLBox);
            this.groupBox1.Location = new System.Drawing.Point(7, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 141);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Request Info";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Request Name:";
            // 
            // rTypeBox
            // 
            this.rTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rTypeBox.FormattingEnabled = true;
            this.rTypeBox.Items.AddRange(new object[] {
            "GET",
            "POST",
            "PUT"});
            this.rTypeBox.Location = new System.Drawing.Point(132, 88);
            this.rTypeBox.Name = "rTypeBox";
            this.rTypeBox.Size = new System.Drawing.Size(255, 28);
            this.rTypeBox.TabIndex = 5;
            // 
            // rNameBox
            // 
            this.rNameBox.Location = new System.Drawing.Point(132, 19);
            this.rNameBox.Name = "rNameBox";
            this.rNameBox.Size = new System.Drawing.Size(255, 26);
            this.rNameBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Request Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Request URL:";
            // 
            // rURLBox
            // 
            this.rURLBox.Location = new System.Drawing.Point(132, 56);
            this.rURLBox.Name = "rURLBox";
            this.rURLBox.Size = new System.Drawing.Size(255, 26);
            this.rURLBox.TabIndex = 3;
            // 
            // rSaveBtn
            // 
            this.rSaveBtn.Location = new System.Drawing.Point(6, 167);
            this.rSaveBtn.Name = "rSaveBtn";
            this.rSaveBtn.Size = new System.Drawing.Size(74, 31);
            this.rSaveBtn.TabIndex = 10;
            this.rSaveBtn.Text = "New";
            this.rSaveBtn.UseVisualStyleBackColor = true;
            this.rSaveBtn.Click += new System.EventHandler(this.rSaveBtn_Click);
            // 
            // rRunBtn
            // 
            this.rRunBtn.Location = new System.Drawing.Point(86, 167);
            this.rRunBtn.Name = "rRunBtn";
            this.rRunBtn.Size = new System.Drawing.Size(74, 31);
            this.rRunBtn.TabIndex = 11;
            this.rRunBtn.Text = "Run";
            this.rRunBtn.UseVisualStyleBackColor = true;
            this.rRunBtn.Click += new System.EventHandler(this.rRunBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 521);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "APIMan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ListBox collectionList;
        private System.Windows.Forms.ComboBox rTypeBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rURLBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox rNameBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox rParamsValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox rParamsName;
        private System.Windows.Forms.ListBox rParamsList;
        private System.Windows.Forms.Button rParamsRemove;
        private System.Windows.Forms.Button rParamsAdd;
        private System.Windows.Forms.Button rRemoveBtn;
        private System.Windows.Forms.Button rAddBtn;
        private System.Windows.Forms.Button rSaveBtn;
        private System.Windows.Forms.Button rRunBtn;
    }
}


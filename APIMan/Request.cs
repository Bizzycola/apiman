﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIMan
{
    /// <summary>
    /// Represents a request parameter
    /// </summary>
   [Serializable]
    internal class RequestParam
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public RequestParam(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return String.Format("{0}={1}", Name, Value);
        }
    }

    /// <summary>
    /// Represents a request
    /// </summary>
    [Serializable]
    internal class Request
    {
        public string Name { get; set; }
        public string URL { get; set; }
        public int Type { get; set; }

        public List<RequestParam> Params { get; set; }

        public Request(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
